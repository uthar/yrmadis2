(defpackage yrmadis2/cubemap
  (:use :cl)
  (:local-nicknames
   (:png :pngload)))

(in-package yrmadis2/cubemap)

(defclass cubemap ()
  ((texture :initform 0)
   (negx :initarg :negx :initform (error "negx required"))
   (negy :initarg :negy :initform (error "negy required"))
   (negz :initarg :negz :initform (error "negz required"))
   (posx :initarg :posx :initform (error "posx required"))
   (posy :initarg :posy :initform (error "posy required"))
   (posz :initarg :posz :initform (error "posz required"))))

(defun bytes-per-pixel (format)
  (case format
    ((:rgb :bgr) 3)
    (:rgba 4)
    (t (error "Format ~a not supported" format))))

(defun gl-texture-format (format)
  (case format
    (:truecolor :rgb)
    (:truecolour :rgb)
    (:indexed-colour :rgb)
    ;; FIXME not sure
    (:truecolour-alpha :rgba)
    (:ycbcr-rgb :bgr)
    (t (error "Format ~a not supported" format))))

(defmethod initialize-instance :after ((cubemap cubemap) &key)
  (with-slots (negx negy negz posx posy posz)
      cubemap
    (let ((texture (gl:gen-texture)))
      (gl:bind-texture :texture-cube-map texture)
      (flet ((load-part (target file)
               (let* ((png (png:load-file file :flatten t))
                      (format (gl-texture-format (pngload:color-type png))))
                 (gl:tex-image-2d target
                                  0
                                  (bytes-per-pixel format)
                                  (png:width png)
                                  (png:height png)
                                  0
                                  format
                                  :unsigned-byte
                                  (png:data png)))))
        (load-part :texture-cube-map-positive-x posx)
        (load-part :texture-cube-map-positive-y posy)
        (load-part :texture-cube-map-positive-z posz)
        (load-part :texture-cube-map-negative-x negx)
        (load-part :texture-cube-map-negative-y negy)
        (load-part :texture-cube-map-negative-z negz))
      (gl:tex-parameter :texture-cube-map :texture-mag-filter :linear)
      (gl:tex-parameter :texture-cube-map :texture-min-filter :linear)
      (gl:tex-parameter :texture-cube-map :texture-wrap-s :clamp-to-edge)
      (gl:tex-parameter :texture-cube-map :texture-wrap-t :clamp-to-edge)
      (gl:tex-parameter :texture-cube-map :texture-wrap-r :clamp-to-edge)
      (gl:bind-texture :texture-cube-map 0)
      (setf (slot-value cubemap 'texture) texture))))
