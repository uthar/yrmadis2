(defpackage yrmadis2/main
  (:use :cl :3d-vectors :3d-matrices)
  (:import-from :alexandria-2 :line-up-first)
  (:local-nicknames
   (:a :alexandria)
   (:sphere :yrmadis2/sphere)
   (:camera :yrmadis2/camera)
   (:cubemap :yrmadis2/cubemap)
   (:shaders :yrmadis2/shaders)
   (:sdl2 :sdl2/all)
   (:gl :cl-opengl))
  (:export
   #:main))

(in-package yrmadis2/main)

(defparameter *width* 800)
(defparameter *height* 600)
(defvar *window* nil)
(defvar *gl* nil)
(defvar *sphere* nil)
(defvar *camera* nil)

(defvar *planets* (make-hash-table :test 'equal))

(defgeneric render (planet))
(defgeneric update (planet))

(defclass earth ()
  (shader
   cubemap
   position
   velocity))

(defmethod initialize-instance ((earth earth) &key)
  (with-slots (shader cubemap position velocity) earth
    (setf position (vec3 -9.0 0 0))
    (setf velocity (vec3 0 0.1 0))
    (setf shader
          (make-instance 'shaders:opengl-shader
                         :vert "src/glsl/earth.vert"
                         :frag "src/glsl/earth.frag"))
    (setf cubemap
          (make-instance 'cubemap::cubemap
                         :posx "./textures/earth/posx.png"
                         :posy "./textures/earth/posy.png"
                         :posz "./textures/earth/posz.png"
                         :negx "./textures/earth/negx.png"
                         :negy "./textures/earth/negy.png"
                         :negz "./textures/earth/negz.png"))))


(defmethod render ((earth earth))
  (with-slots (shader cubemap position) earth
    (let ((model (m*
                  (mtranslation position)
                  (mscaling (vec3 0.8 0.8 0.8))
                  (mrotation +vx+ (camera::rad 30))
                  (mrotation +vy+ (let ((time (sdl2:get-ticks)))
                                    (* 0.03 (camera::rad time))))))
          (view (camera:view-matrix *camera*))
          (projection (mperspective (slot-value *camera* 'camera::zoom)
                                    (/ *width* *height*)
                                    0.1 100.0)))
      (gl:use-program (slot-value shader 'shaders::program))
      (gl:bind-texture :texture-cube-map (slot-value cubemap 'cubemap::texture))
      (gl:uniformfv (gl:get-uniform-location
                     (slot-value shader 'shaders::program)
                     "sun.position")
                    (let* ((sun (gethash "sun" *planets*))
                           (position (slot-value sun 'position)))
                      (vector (vx position) (vy position) (vz position))))
      (gl:uniformfv (gl:get-uniform-location
                     (slot-value shader 'shaders::program)
                     "sun.ambient")
                    (vector 0.1 0.1 0.1))
      (gl:uniformfv (gl:get-uniform-location
                     (slot-value shader 'shaders::program)
                     "sun.diffuse")
                    (vector 1.0 1.0 1.0))
      (gl:uniform-matrix-4fv (gl:get-uniform-location
                              (slot-value shader 'shaders::program)
                              "model")
                             (marr model))
      (gl:uniform-matrix-3fv (gl:get-uniform-location
                              (slot-value shader 'shaders::program)
                              "normal")
                             (line-up-first
                              model
                              (minv)
                              (mtranspose)
                              (mblock 0 0 3 3)
                              (marr)))
      (gl:uniform-matrix-4fv (gl:get-uniform-location
                              (slot-value shader 'shaders::program)
                              "view")
                             (marr view))
      (gl:uniform-matrix-4fv (gl:get-uniform-location
                              (slot-value shader 'shaders::program)
                              "projection")
                             (marr projection)))
    (gl:bind-vertex-array (slot-value *sphere* 'sphere::vao))
    (gl:polygon-mode :front-and-back :fill)
    (gl:draw-arrays :triangles 0 (* 3 (length (slot-value *sphere* 'sphere::triangles))))))

(declaim (optimize debug))

(defmethod update ((earth earth))
  (let* ((sun (gethash "sun" *planets*))
         (direction (v- (slot-value sun 'position)
                        (slot-value earth 'position)))
         (distance (vlength direction))
         (dropoff (min 1 (expt (/ 1 distance) 2)))
         (acceleration (v* (vunit direction)
                           (slot-value sun 'gravity)
                           dropoff)))
    (with-slots (position velocity) earth
      (setf velocity (v+ velocity acceleration))
      (setf position (v+ position velocity)))))

(defclass sun ()
  (shader
   cubemap
   position
   gravity))

(defmethod initialize-instance ((sun sun) &key)
  (with-slots (shader cubemap position gravity) sun
    (setf position (vec3 2.0 0 0))
    (setf gravity 0.1)
    (setf shader
          (make-instance 'shaders:opengl-shader
                         :vert "src/glsl/sun.vert"
                         :frag "src/glsl/sun.frag"))
    (setf cubemap
          (make-instance 'cubemap::cubemap
                         :posx "./textures/sun/px.png"
                         :posy "./textures/sun/py.png"
                         :posz "./textures/sun/pz.png"
                         :negx "./textures/sun/nx.png"
                         :negy "./textures/sun/ny.png"
                         :negz "./textures/sun/nz.png"))))

(defmethod render ((sun sun))
  (with-slots (shader cubemap position) sun
    (gl:use-program (slot-value shader 'shaders::program))
    (gl:bind-texture :texture-cube-map (slot-value cubemap 'cubemap::texture))
    (gl:uniform-matrix-4fv (gl:get-uniform-location
                            (slot-value shader 'shaders::program)
                            "model")
                           (marr
                            (m*
                             (mtranslation position)
                             (mscaling (vec3 1.8 1.8 1.8))
                             (mrotation +vy+ (let ((time (sdl2:get-ticks)))
                                               (* 0.03 (camera::rad time)))))))
    (let ((view (camera:view-matrix *camera*))
          (projection (mperspective (slot-value *camera* 'camera::zoom)
                                    (/ *width* *height*)
                                    0.1 100.0)))
      (gl:uniform-matrix-4fv (gl:get-uniform-location
                              (slot-value shader 'shaders::program)
                              "view")
                             (marr view))
      (gl:uniform-matrix-4fv (gl:get-uniform-location
                              (slot-value shader 'shaders::program)
                              "projection")
                             (marr projection)))
    (gl:bind-vertex-array (slot-value *sphere* 'sphere::vao))
    (gl:polygon-mode :front-and-back :fill)
    (gl:draw-arrays :triangles
                    0
                    (* 3 (length (slot-value *sphere* 'sphere::triangles))))))

(defmethod update ((sun sun))
  (nv+ (slot-value sun 'position) (vec3 0 0 0)))

(defun init ()
  (sdl2:init '(:everything))
  ;; anti-aliasing
  ;; (sdl2:gl-set-attribute :multisamplebuffers 1)
  ;; (sdl2:gl-set-attribute :multisamplesamples 4)
  ;; (sdl2:gl-set-attribute :accelerated-visual 1)
  (setf *window* (sdl2:create-window "Yrmadis" 0 0 *width* *height* '(:opengl)))
  (setf *gl* (sdl2:gl-create-context *window*))
  (sdl2:gl-make-current *window* *gl*)
  (gl:enable :depth-test)
  (gl:enable :multisample)
  (setf *camera* (camera:camera 0 0 30))
  (setf *sphere* (sphere:load-sphere 5))
  (setf (gethash "earth" *planets*) (make-instance 'earth))
  (setf (gethash "sun" *planets*) (make-instance 'sun))
  (values))

(defun cleanup ()
  (sdl2:gl-make-current *window* *gl*)
  (dolist (planet (a:hash-table-values *planets*))
    (with-slots (shader cubemap) planet
      (gl:delete-texture (slot-value cubemap 'cubemap::texture))
      (gl:delete-program (slot-value shader 'shaders::program))))
  (gl:delete-buffers (list (slot-value *sphere* 'sphere::vao)))
  (sdl2:gl-delete-context *gl*)
  (sdl2:gl-reset-attributes)
  (sdl2:destroy-window *window*)
  (values))

(defparameter *walk-speed* 0.1)

(defvar *pressed-down* nil
  "Buttons currently pressed down")

(defun update-and-render ()
  (when (find :up *pressed-down*)
    (camera:walk *camera* :z (- *walk-speed*)))
  (when (find :down *pressed-down*)
    (camera:walk *camera* :z (+ *walk-speed*)))
  (when (find :left *pressed-down*)
    (camera:walk *camera* :x (+ *walk-speed*)))
  (when (find :right *pressed-down*)
    (camera:walk *camera* :x (- *walk-speed*)))
  (gl:viewport 0 0 *width* *height*)
  (gl:clear-color 0.1 0.1 0.1 1.0)
  (gl:clear :color-buffer-bit :depth-buffer-bit)
  (dolist (planet (a:hash-table-values *planets*))
    (update planet)
    (render planet)
    )
  (gl:bind-vertex-array 0)
  (gl:bind-texture :texture-cube-map 0)
  (gl:use-program 0)
  (sdl2:gl-swap-window *window*)
  (values))

(defmacro while (test &body body)
  `(do () ((not ,test)) ,@body))

(defgeneric handle-event (event-type event))

(defmethod handle-event ((event-type t) event)
  (format t "Ignoring event of type ~A~%" event-type)
  (values))

(defmethod handle-event ((event-type (eql :keyup)) event)
  (let* ((keysym (cffi:foreign-slot-pointer
                  event
                  '(:struct sdl2:keyboard-event)
                  'sdl2:keysym))
         (keycode (cffi:foreign-slot-value
                   keysym
                   '(:struct sdl2:keysym)
                   'sdl2/keyboard::keycode)))
    (alexandria:removef *pressed-down* keycode)
    (format t "~A~%" *pressed-down*)
    (values)))

(defmethod handle-event ((event-type (eql :keydown)) event)
  (let* ((keysym (cffi:foreign-slot-pointer
                  event
                  '(:struct sdl2:keyboard-event)
                  'sdl2:keysym))
         (keycode (cffi:foreign-slot-value
                   keysym
                   '(:struct sdl2:keysym)
                   'sdl2/keyboard::keycode)))
    (pushnew keycode *pressed-down*)
    (format t "~A~%" *pressed-down*)
    (values)))

(defmethod handle-event ((event-type (eql :quit)) event)
  (invoke-restart 'quit))

(defun handle-input ()
  (let ((event (sdl2:make-event)))
    (unwind-protect
         (while (plusp (sdl2:poll-event event))
           (let ((event-type (cffi:mem-ref event 'sdl2:event-type)))
             (handle-event event-type event)))
      (cffi:foreign-free event))))
 

(defun main ()
  (init)
  (unwind-protect
       (loop
        (restart-case
            (progn
              (handle-input)
              (update-and-render)
              (sleep 1/60))
          (quit ()
            :report "Quit the main loop"
            (return))
          (skip-frame ()
            :report "Skip this frame")))
    (cleanup)))
     
                  
