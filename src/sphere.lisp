(defpackage yrmadis2/sphere
  (:use :cl :3d-vectors)
  (:export
   #:load-sphere))

(in-package yrmadis2/sphere)

(defclass sphere ()
  ((precision :initarg :precision)
   (triangles :initarg :triangles)
   (vao :initarg :vao)))

(defun split-triangle (triangle)
  (destructuring-bind (a b c)
      triangle
    (let ((x (v+ a (v* (v- b a) 1/2)))
          (y (v+ b (v* (v- c b) 1/2)))
          (z (v+ c (v* (v- a c) 1/2))))
      (list
       (list a x z)
       (list x b y)
       (list x z y)
       (list c y z)))))

(defun split-triangles (triangles)
  (apply 'concatenate 'list (map 'list 'split-triangle triangles)))

(defparameter *center* (vec3 0 0 0))

(defun normalize-triangle (triangle)
  (destructuring-bind (a b c)
      triangle
    (let ((x (vunit (v- a *center*)))
          (y (vunit (v- b *center*)))
          (z (vunit (v- c *center*))))
      (list x y z))))

(defun make-sphere-triangles (precision)
  (loop repeat precision
        for triangles = (list (list (vec3 0 1 0)
                                    (vec3 1 0 -1)
                                    (vec3 1 0 1))
                              (list (vec3 0 1 0)
                                    (vec3 -1 0 1)
                                    (vec3 1 0 1))
                              (list (vec3 0 1 0)
                                    (vec3 -1 0 -1)
                                    (vec3 -1 0 1))
                              (list (vec3 0 1 0)
                                    (vec3 1 0 -1)
                                    (vec3 -1 0 -1))
                              (list (vec3 0 -1 0)
                                    (vec3 1 0 -1)
                                    (vec3 1 0 1))
                              (list (vec3 0 -1 0)
                                    (vec3 -1 0 1)
                                    (vec3 1 0 1))
                              (list (vec3 0 -1 0)
                                    (vec3 -1 0 -1)
                                    (vec3 -1 0 1))
                              (list (vec3 0 -1 0)
                                    (vec3 1 0 -1)
                                    (vec3 -1 0 -1)))
          then (split-triangles triangles)
        finally (return (map 'list 'normalize-triangle triangles))))

(defun load-sphere (precision)
  (let* ((vao (gl:gen-vertex-array))
         (vbo (gl:gen-buffer))
         (triangles (make-sphere-triangles precision))
         (vertices (gl:alloc-gl-array :float (* 9 (length triangles)))))
    (unwind-protect
         (progn
           (gl:bind-vertex-array vao)
           (gl:bind-buffer :array-buffer vbo)
           (let ((index -1))
             (dolist (triangle triangles)
               (setf (gl:glaref vertices (incf index)) (vx (first triangle))
                     (gl:glaref vertices (incf index)) (vy (first triangle))
                     (gl:glaref vertices (incf index)) (vz (first triangle))
                     (gl:glaref vertices (incf index)) (vx (second triangle))
                     (gl:glaref vertices (incf index)) (vy (second triangle))
                     (gl:glaref vertices (incf index)) (vz (second triangle))
                     (gl:glaref vertices (incf index)) (vx (third triangle))
                     (gl:glaref vertices (incf index)) (vy (third triangle))
                     (gl:glaref vertices (incf index)) (vz (third triangle)))))
           (gl:buffer-data :array-buffer :static-draw vertices)
           (gl:vertex-attrib-pointer 0 3 :float nil
                                     (* (cffi:foreign-type-size :float) 3)
                                     (* (cffi:foreign-type-size :float) 0))
           (gl:enable-vertex-attrib-array 0)
           (make-instance 'sphere :vao vao
                                  :triangles triangles
                                  :precision precision))
      (gl:free-gl-array vertices)
      (gl:bind-vertex-array 0)
      (gl:bind-buffer :array-buffer 0))))
