(defun print-gc-time ()
  (format t "GC took ~a milliseconds~%" (* 1000.0
                                           (/ sb-ext:*gc-run-time*
                                              internal-time-units-per-second)))
  (setf sb-ext:*gc-run-time* 0))

(pushnew 'print-gc-time sb-ext:*after-gc-hooks*)
