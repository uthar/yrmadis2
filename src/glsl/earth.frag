#version 130

in vec4 fragmentPosition;
in vec3 fragmentDirection;
in vec3 fragmentDirectionNormal;

out vec4 fragmentColor;

uniform samplerCube day;

// TODO night lights texture
// uniform samplerCube night;

struct Sun {
  vec3 position;
  vec3 ambient;
  vec3 diffuse;
};

uniform Sun sun;

vec3 calcSunLight() {

  vec3 color = vec3(texture(day, fragmentDirection));

  vec3 ambient = sun.ambient * color;

  vec3 lightDirection = normalize(sun.position - fragmentPosition.xyz);

  float cosine = dot(lightDirection, normalize(fragmentDirectionNormal));
  
  vec3 diffuse = max(cosine, 0.0) * sun.diffuse * color;

  return ambient + diffuse;
}


void main() {

  vec3 color = calcSunLight();

  fragmentColor = vec4(color, 1.0);
}
