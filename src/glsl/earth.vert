#version 130

in vec3 vertexPosition;

out vec4 fragmentPosition;
out vec3 fragmentDirection;
out vec3 fragmentDirectionNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat3 normal;

void main() {
  gl_Position = projection * view * model * vec4(vertexPosition, 1.0);
  fragmentDirection = vertexPosition;
  fragmentDirectionNormal = normal * vertexPosition;
  fragmentPosition = model * vec4(vertexPosition, 1.0);
}
