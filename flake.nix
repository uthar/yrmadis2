{
  description = "Lisp env";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs?ref=nixos-24.11";

  outputs = { self, nixpkgs }: let inherit (nixpkgs) lib; in {
    devShell = lib.genAttrs ["x86_64-linux" "aarch64-linux" "aarch64-darwin"] (system:
      let
        pkgs = import nixpkgs { inherit system; config = {}; overlays = []; };
        lisp = lib.pipe pkgs.sbcl [
          (l: l.withPackages (ps: builtins.attrValues {
            inherit (ps)
              alexandria
              cffi
              _3d-quaternions
              cl-opengl
              pngload
            ;
          }))
        ];
      in pkgs.mkShellNoCC {
        buildInputs = [ lisp ];
        shellHook = ''
          export LD_LIBRARY_PATH=${pkgs.SDL2}/lib
          export DYLD_LIBRARY_PATH=${pkgs.SDL2}/lib
        '';
      });
  };
}
