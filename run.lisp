;; To start rendering the solar system:
;;
;; First, run direnv allow on a GNU/Linux system with Nix installed
;;
;; Then, run:
(flet ((getenv (x)
         #+sbcl (sb-ext:posix-getenv x)
         #+ecl (ext:getenv x)))
  (load (getenv "ASDF")))
(push (uiop:getcwd) asdf:*central-registry*)
(asdf:load-system 'yrmadis2)
(yrmadis2/main:main)
